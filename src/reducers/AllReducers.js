export const reposReducer = (repos=[], action) => {
    if (action.type === 'FETCH_REPOS'){
        return action.payload;
    }
    return repos;
};

export const pageNumReducer = (page_num = 0, action) => {
    if (action.type === 'PAGE_NUM_UPDATE') {
        return action.payload;
    }
    return page_num;
};

export const searchTypeReducer = () => {
    return {new_search: 0, prev: -1, next: 1};   
}

export const packageStatsReducer = (packageStats = [], action) => {
    if (action.type === 'PACKAGE_STATS_UPDATE'){
        return action.payload;
    }
    return packageStats;
}

export const reposIdsReducer = (reposIds = new Set(), action) => {
    if (action.type === 'REPOS_IDS_UPDATE'){
        return new Set( [... reposIds, action.payload] ) ;
    }
    return reposIds;
}