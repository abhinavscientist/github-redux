import { combineReducers } from 'redux';
import { reposReducer } from './AllReducers';
import { pageNumReducer } from './AllReducers';
import { searchTypeReducer } from './AllReducers';
import { packageStatsReducer } from './AllReducers';
import { reposIdsReducer } from './AllReducers';

const reducers = combineReducers({
    repos : reposReducer,
    page_num : pageNumReducer,
    search_type_obj: searchTypeReducer,
    packageStats: packageStatsReducer,
    reposIds: reposIdsReducer
});


export default reducers;