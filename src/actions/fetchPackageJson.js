import _ from 'lodash';
import axios from 'axios';

export const fetchPackageJson = function (id, url) {
    return function (dispatch, getState) {
        const { reposIds, packageStats } = getState();
        if (!reposIds.has(id)) {
            axios({
                method: 'get',
                url: url,
            })
            .then((response) => {
                try {
                    if (response.data.encoding === 'base64') {
                        const content = atob(response.data.content)
                        const devDependencies = JSON.parse(content).devDependencies;
                        const packagesKeysList = Object.keys(devDependencies);
                        let tempPackages = Object.assign({}, packageStats);
                        packagesKeysList.forEach((value) => {
                            if (value in tempPackages) {
                                tempPackages[value] = tempPackages[value] + 1;
                            } else {
                                tempPackages[value] = 1;
                            }
                        });
                        dispatch({ type: 'PACKAGE_STATS_UPDATE', payload: tempPackages });
                        dispatch({ type: 'REPOS_IDS_UPDATE', payload: id });
                    }
                }
                catch (error) {
                    console.log('Error in processing base64 blob content');
                    console.error(error);
                }
            })
            .catch((response) => {
                dispatch({ type: 'REPOS_IDS_UPDATE', payload: id });
                window.alert('Repository does not contain package.json file');
            });
        }
    }
};
