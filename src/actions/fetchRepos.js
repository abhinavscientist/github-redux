import gitHubApiSetting from '../apis/gitHubApiSetting';

const pageNumUpdate = function(payload){
    return function(dispatch) {
        dispatch({type: 'PAGE_NUM_UPDATE', payload: payload});
    }
};

export const fetchRepos = function(query = '', search_type=0) {
    return async function(dispatch, getState) {
        const { page_num, search_type_obj } = getState();
        const params = {
            q: query,
            sort: 'forks',
            order: 'desc',
            per_page: 10,
            page: page_num + search_type
        };
        gitHubApiSetting.get('search/repositories',{params: params})
        .then((response) => {
            if (response.data.items.length !== 0){
                dispatch({type: 'FETCH_REPOS', payload: response.data.items});
            }
            if (search_type === search_type_obj.new_search){
                dispatch(pageNumUpdate(1));
            }
            if (search_type === search_type_obj.next){
                dispatch(pageNumUpdate(page_num + 1));
            }
            if (search_type === search_type_obj.prev && page_num > 1){
                dispatch(pageNumUpdate(page_num - 1)); 
            }
        })
        .catch((error) => {
            console.error(error);
        });
        
    }
};