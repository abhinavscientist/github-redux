import React from 'react';
import { connect } from 'react-redux';
import { fetchPackageJson } from '../actions/fetchPackageJson'

class UserGitHubProfile extends React.Component{
    fetchJson = (event) => {
        const { repoid, json_url } = this.props.urlIdObject;
        this.props.fetchPackageJson(repoid, json_url);
    }

    render() {
        const { repoid } = this.props.urlIdObject;
        const isBtnDisabled = this.props.reposIds.has(repoid);
        
        const {owner, name, description, forks_count} = this.props.item;
        return (
            <div className="ui items">
                <div className="item">
                    <div className="image">
                        <img alt="" src={owner.avatar_url} />
                    </div>
                    <div className="content">
                        <a className="header" href="/">{`Owner: ${name}`}</a>
                        <div className="meta">
                            <span>{description}</span>
                        </div>
                        <div className="description">
                            <p></p>
                        </div>
                        <div className="extra">
                            {`Fork Count: ${forks_count}`}
                        </div>
                        <div>
                            <button disabled={isBtnDisabled} onClick={this.fetchJson}>Import</button>
                        </div>
                    </div>
                </div>
            </div>
        );
    }
}


const mapStateToProp = (state, ownProps) => {
    const { reposIds } = state;
    return {
        reposIds,
        item: ownProps.item,
        urlIdObject: ownProps.urlIdObject
    };
}

export default connect(mapStateToProp, { fetchPackageJson })(UserGitHubProfile);