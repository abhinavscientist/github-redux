import React from 'react';
import SearchBar from './SearchBar';
import UserGitHubProfileList from './UserGitHubProfileList';
import PackageStats from './PackageStats';

class App extends React.Component{
    render() {
        return (
            <div className="container-fluid">
                <SearchBar></SearchBar>
                <div className="container-fluid">
                    <div className="row">
                        <div className="col-8 border-right border-top" >
                            <h3><u>Repo Search Results</u></h3>
                            <UserGitHubProfileList></UserGitHubProfileList>
                        </div>
                        <div className="col-4 border-top">
                            <h3><u>Package Statistics</u></h3>
                            <PackageStats></PackageStats>
                        </div>
                    </div>
                </div>
            </div>
        );
    }
}

export default App;