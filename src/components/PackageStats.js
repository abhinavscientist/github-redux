import _ from 'lodash';
import React from 'react';
import { connect } from 'react-redux';

class PackageStats extends React.Component{
    
    sortedTopTen = () => {
        const sorted = _.toPairs(this.props.packageStats).sort(function(first, second){
            return second[1] - first[1]
        });
        return sorted.slice(0,10);
    }
    
    render(){
        const sortedTopTen = this.sortedTopTen();
        var rows = [];
        sortedTopTen.forEach((element) => {
            rows.push(<li key={element[0]}>{`${element[0]}: ${element[1]}`}</li>)
        });
        return (
                <ul>
                    {rows}
                </ul>
        );
    }
}

const mapStateToProp = (state) => {
    const { packageStats } = state;
    return {
        packageStats
    };
}

export default connect(mapStateToProp)(PackageStats);