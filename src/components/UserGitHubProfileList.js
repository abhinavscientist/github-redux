import React from 'react';
import UserGitHubProfile from './UserGitHubProfile';
import { connect } from 'react-redux';

const UserGitHubProfileList = (props) => {
    const repos = props.repos.map((current_repo) => {
        const urlIdObject = {};
        try{
            // Building package.json url
            urlIdObject['json_url'] = current_repo.contents_url.split('/').slice(0,-1).concat('package.json').join('/');
            urlIdObject['repoid'] = current_repo.id;
        }
        catch(error){
            console.log('Error in building url for package.json ');
            console.error(error);
        }
        return <UserGitHubProfile key={current_repo.id} item={current_repo} urlIdObject={urlIdObject}></UserGitHubProfile>
    });
    return <div className="repo-list">{repos}</div>
};

const mapStateToProp = (state) => {
    return { repos: state.repos };
}

export default connect(mapStateToProp)(UserGitHubProfileList);